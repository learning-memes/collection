{-# LANGUAGE DeriveFunctor              #-}

{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeApplications           #-}

module Collect (runCollector) where

import           Control.Concurrent       (forkIO, threadDelay)
import           Control.Concurrent.Async (async, wait)
import Control.Concurrent.Chan
import           Control.Monad            (forM, forM_, forever, join, mapM_,
                                           void)
import           Control.Monad.Catch      (MonadThrow)
import           Control.Monad.IO.Class   (MonadIO, liftIO)
import           Control.Monad.Reader     (MonadReader, ReaderT, ask,
                                           runReaderT)
import           Data.Aeson               (ToJSON (..), FromJSON(..), encode, eitherDecode)
import           Data.ByteString.Lazy     (ByteString, appendFile, readFile)
import           Data.Proxy               (Proxy (..))
import           Data.Semigroup           ((<>))
import           Prelude                  hiding (appendFile, readFile)

import           Collect.Has              (Has (..))
import           Collect.Source           (PullFreq (..), Source (..))
import System.Log.FastLogger
import Data.Text hiding (length)

newtype CollectM env a
  = CollectM
  { runCollect :: ReaderT env IO a
  } deriving (Functor, Applicative, Monad, MonadIO, MonadThrow, MonadReader env)

evalCollect :: env -> CollectM env a -> IO a
evalCollect env c = runReaderT (runCollect c) env

handleLogs logChan = do
  logger <- newFileLoggerSet defaultBufSize "logs.txt"
  forever . liftIO
    . pushLogStrLn logger . toLogStr =<< readChan logChan

startCollection
  :: (FromJSON a, ToJSON b, Show b, Source c (CollectM c) b a)
  => p a
  -> p b
  -> FilePath
  -> CollectM c ()
startCollection (_ :: p a) (_ :: p b) pathToSources = do
  env <- ask
  logChan <- liftIO newChan
  liftIO . forkIO $ (handleLogs logChan)
  eitherDecode @[a] <$> liftIO (readFile pathToSources) >>= \case
    Left e -> liftIO . putStrLn $ "Failed to load sources!: " <> e
    Right xs -> liftIO $ do
      threads <- forM xs $
        async . evalCollect env . (collect logChan (Proxy @b))
      forM_ threads wait

collect
  :: (ToJSON b, Show b, Source env (CollectM env) b a)
  => Chan Text
  -> p b
  -> a 
  -> CollectM env ()
collect logChan (_ :: p b) src = forever $ do
  items <- pull @_ @_ @b src
  liftIO $ do
    appendFile "data.json" . (<>) "\n" . encode $ items
    writeChan logChan . pack $
      "pulled " <> show (length items) <> " items."
    let (PullFreq waitTime) = get src
    threadDelay (fromIntegral waitTime)

runCollector :: (Source env (CollectM env) b a, Show b, ToJSON b, FromJSON a)
  => p a -> p b -> FilePath -> env -> IO () 
runCollector a b pathToSources config
  = evalCollect config (startCollection a b pathToSources) 
