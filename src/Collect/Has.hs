{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE UndecidableInstances   #-}

module Collect.Has where

class Has a b | a -> b where
  get :: a -> b

instance Has (a, b) a where
  get (a, _) = a

instance Has (a, b) b where
  get (_, b) = b

class From a b where
  from :: a -> Maybe b
