{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}

module Collect.Meme where

import           Data.Aeson
import           Data.Text
import           Data.Time.Calendar
import           Data.Time.Clock
import           GHC.Generics
import           Numeric.Natural

data Meme
  = Meme
  { originUrl   :: Text
  , imageUrl    :: Text
  , publishTime :: Maybe UTCTime
  , memeTitle   :: Maybe Text
  } deriving (Show, Eq, Generic, ToJSON, FromJSON)
