{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}

module Collect.Source where

import           Control.Monad.Reader
import           Data.Aeson
import           Data.Time
import           GHC.Generics
import           Numeric.Natural
import           Data.Text
import           Data.Typeable
import Control.Monad.Catch

import           Collect.Has

newtype PullFreq
  = PullFreq Natural
    deriving (Show, Eq, Ord, Generic, ToJSON, FromJSON)

class
  ( Has a PullFreq
  , MonadReader env m
  , Monad m
  ) => Source env m b a where
  pull :: a -> m [b]

data Error
  = LoginFailure Text
  | GatherFailure Text
    deriving (Typeable, Show)

instance Exception Error
